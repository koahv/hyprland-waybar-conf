# hyprland-waybar-conf

custom configs for
 - hyprland
 - hyprpaper
 - waybar
 - wofi
 - workstyle
  - foot

![dwm style](https://gitlab.com/koahv/hyprland-waybar-conf/-/raw/main/screenshot_02.png)
![wofi](https://gitlab.com/koahv/hyprland-waybar-conf/-/raw/main/screenshot_01.png)

